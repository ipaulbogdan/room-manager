import {Component, OnInit} from '@angular/core';
import {Time} from 'src/app/models/time';
import {Date} from 'src/app/models/date';
import {toBeSent} from 'src/app/models/to-be.sent';
import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-request-room',
  templateUrl: './request-room.component.html',
  styleUrls: ['./request-room.component.css']
})
export class RequestRoomComponent implements OnInit {

  model: requestRoomViewModel = {
    eventName: '',
    eventAuthor: '',
    personCount: '',
    startingTime: '',
    endingTime:  '',
    eventDate: ''
  };


  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }

  sendFeedBack(): void {
    const startingTime = new Time();
    const endingTime = new Time();
    const date = new Date();
    console.log(this.model.eventDate);
    date.getDate(this.model.eventDate);
    startingTime.getTime(this.model.startingTime);
    endingTime.getTime(this.model.endingTime);
    const send = new toBeSent(this.model.eventName, this.model.eventAuthor, +this.model.personCount, startingTime, endingTime, date);
    console.log(send);
    this.http.post('http://localhost:8080/requestRoom', send).subscribe(
        res => {
          location.reload();
        },
          err => {
            alert('There is no room for this event!');
          }
    );
  }


}


export interface requestRoomViewModel {
    eventName: string;
    eventAuthor: string;
    personCount: string;
    startingTime: string;
    endingTime: string;
    eventDate: string;
}

