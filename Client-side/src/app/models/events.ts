import {Time} from './time';
import {Date} from './date';

export class Event {
  public id: string;
  public roomName: string;
  public eventName: string;
  public eventAuthor: string;
  public startingTime: Time;
  public endingTime: Time;
  public eventDate: Date;
  public checkIn: boolean;

  constructor() {
    this.id = 'dasdasdad';
    this.eventName = 'Currently there is no meeting!';
    this.eventAuthor = '';
    this.startingTime = new Time();
    this.endingTime = new Time();
    this.eventDate = new Date();
    this.checkIn = false;
  }
}
