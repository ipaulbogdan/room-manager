import {Time} from './time';
import {Date} from './date';

export class toBeSent {

  eventName: string;
  eventAuthor: string;
  personCount: number;
  startingTime: Time;
  endingTime: Time;
  eventDate: Date;

  constructor(eventName: string, authorName: string, personCount: number, startingTime: Time, endingTime: Time, eventDate: Date ) {
        this.eventName = eventName;
        this.eventAuthor = authorName;
        this.personCount = personCount;
        this.startingTime = startingTime;
        this.endingTime = endingTime;
        this.eventDate = eventDate;
  }

}
