export class Time {
  hour: number;
  minutes: number;

  constructor() {
    this.hour = 0;
    this.minutes = 0;
  }


  getTime(time: string): void {
    const split = time.split(':', 2);
    this.hour = +split[0];
    this.minutes = +split[1];
  }

}
