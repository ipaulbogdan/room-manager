export class Date {
  day: number;
  month: number;
  year: number;

  constructor() {
    this.day = 0;
    this.month = 0;
    this.year = 0;
  }

  getDate(date: string): void{
    const split = date.split('-', 3);
    this.day = +split[2];
    this.month = +split[1];
    this.year = +split[0];
  }
}
