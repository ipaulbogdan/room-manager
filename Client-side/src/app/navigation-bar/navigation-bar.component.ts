import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {
  isVisible: boolean;

  constructor() {
  }

  ngOnInit() {
    this.isVisible = true;
  }

}

function hide() {
  this.isVisible = false;
}


