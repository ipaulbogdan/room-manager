import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import {RequestRoomComponent, requestRoomViewModel} from './request-room/request-room.component';
import { NotFoundComponent } from './not-found/not-found.component';
import {Router, RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ViewMarpleComponent } from './view-maple/view-marple.component';
import { ViewD1Component } from './view-d1/view-d1.component';
import { ViewClujComponent } from './view-cluj/view-cluj.component';

const appRoutes: Routes = [
  {
    path: 'request',
    component: RequestRoomComponent
  },
  {
    path: 'view/Cluj',
    component: ViewClujComponent
  },
  {
    path: 'view/Maple',
    component: ViewMarpleComponent
  },
  {
    path: 'view/D1',
    component: ViewD1Component
  },
  {
    path: '',
    component: RequestRoomComponent,
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent

  }
]

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    RequestRoomComponent,
    NotFoundComponent,
    ViewMarpleComponent,
    ViewD1Component,
    ViewClujComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
