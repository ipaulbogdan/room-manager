import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Event} from 'src/app/models/events';
import {Time} from '@angular/common';


@Component({
  selector: 'app-view-d1',
  templateUrl: './view-d1.component.html',
  styleUrls: ['./view-d1.component.css']
})
export class ViewD1Component implements OnInit {
  currentEvent: Event;
  nextEvent: Event;
  constructor(private http: HttpClient) {
    this.currentEvent = new Event();
    this.nextEvent = new Event();
    this.getEvents();

  }

  ngOnInit() {
  }

  getEvents() {
    const currentDate = new Date();
    this.http.get('http://localhost:8080/D1/todayEvents').subscribe(
      (events: Event[]) => {
        if (events.length >= 2) {
          console.log(events);
          if (events[0].startingTime.hour <= currentDate.getHours() &&  events[0].startingTime.minutes <= currentDate.getMinutes()) {
            this.currentEvent = events[0];
            this.nextEvent = events[1];
          } else {
            this.nextEvent = events[0];
          }
        } else if (events.length === 1) {
          if (events[0].startingTime.hour <= currentDate.getHours() && events[0].startingTime.minutes <= currentDate.getMinutes()) {
            this.currentEvent = events[0];
            this.nextEvent.eventName = ' No upcoming meeting!';
          } else {
            this.nextEvent = events[0];

          }
        } else {
          this.nextEvent = new Event();
          this.currentEvent = new Event();
          this.nextEvent.eventName = 'No upcoming meeting!';
        }
      }
    );
  }

  checkIn(button) {
    this.http.post('http://localhost:8080/room/checkin', this.currentEvent).subscribe(
      res => {
        this.currentEvent.checkIn = true;
      },
      err => {
        console.log('No checkin efected on');
      }
    );
  }
}
