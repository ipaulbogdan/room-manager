package com.idorasi.server.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.text.SimpleDateFormat;
import java.util.Calendar;


@Document(collection = "Events")
public class Event {

    @Id
    private String id;
    private String roomName;

    private String eventName;
    private String eventAuthor;
    private int personCount;
    private Time startingTime;
    private Time endingTime;
    private Date eventDate;
    private boolean checkIn=false;


    public Event(String eventName,
                      String eventAuthor,
                      int personCount,
                      Time startingTime,
                      Time endingTime,
                      Date eventDate){
        this.eventName = eventName;
        this.eventAuthor = eventAuthor;
        this.personCount = personCount;
        this.startingTime=startingTime;
        this.endingTime=endingTime;
        this.eventDate=eventDate;
    }

    public Event(){};


    public boolean verifyExpiration() {
        Time currentTime = new Time();
        currentTime.getCurrentTime();

        if(!currentTime.greaterThan(startingTime)){
            return false;
        }

        if (currentTime.getHour() == startingTime.getHour()) {
            if (currentTime.getMinutes() < (startingTime.getMinutes() + 5) && currentTime.getMinutes() > startingTime.getMinutes())
                return false;
        }
        if(currentTime.greaterThan(endingTime))
            return true;

        return !checkIn;
    }




    public void checkIn(){
        this.checkIn=true;
    }

    public boolean isCheckIn() {
        return checkIn;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventAuthor() {
        return eventAuthor;
    }

    public void setEventAuthor(String eventAuthor) {
        this.eventAuthor = eventAuthor;
    }

    public int getPersonCount() {
        return personCount;
    }

    public void setPersonCount(int personCount) {
        this.personCount = personCount;
    }

    public Time getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(Time startingTime) {
        this.startingTime = startingTime;
    }

    public Time getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(Time endingTime) {
        this.endingTime = endingTime;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
