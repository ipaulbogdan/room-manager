package com.idorasi.server.models;


import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Date {


    private int day;
    private int month;
    private int year;

    public Date() {
    }

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }


    public boolean equals(Object o){
        if(o instanceof Date)
            return ((Date)o).getDay()==this.day && ((Date)o).getMonth()==this.month && ((Date)o).getYear()==this.year;
        return false;
    }

    public void getCurrentDate(){
        int day= Integer.parseInt(new SimpleDateFormat("dd").format(Calendar.getInstance().getTime()));
        int month= Integer.parseInt(new SimpleDateFormat("MM").format(Calendar.getInstance().getTime()));
        int year= Integer.parseInt(new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime()));

        this.setDay(day);
        this.setMonth(month);
        this.setYear(year);
    }


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}

