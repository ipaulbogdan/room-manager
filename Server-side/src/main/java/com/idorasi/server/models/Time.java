package com.idorasi.server.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Time {

    private int hour;
    private int minutes;

    public Time(int hour,int minutes){
        this.hour=hour;
        this.minutes=minutes;
    }

    public Time(){ }


    public void getCurrentTime(){
        int hour= Integer.parseInt(new SimpleDateFormat("HH").format(Calendar.getInstance().getTime()));
        int minutes= Integer.parseInt(new SimpleDateFormat("mm").format(Calendar.getInstance().getTime()));

        setHour(hour);
        setMinutes(minutes);
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public boolean equals(Object o){
        if(o instanceof Time)
            return ((Time)o).hour==hour && ((Time)o).minutes==minutes;
        return false;
    }

    public boolean greaterThan(Time timp){
        return this.hour > timp.getHour() || (this.hour == timp.getHour() && this.minutes > timp.getMinutes());
    }



}
