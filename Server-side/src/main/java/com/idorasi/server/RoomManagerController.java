package com.idorasi.server;

import com.idorasi.server.Exceptions.HourException;
import com.idorasi.server.Exceptions.NegativeException;
import com.idorasi.server.database.EventRepository;
import com.idorasi.server.models.Date;
import com.idorasi.server.models.Event;
import com.idorasi.server.models.Time;
import com.idorasi.server.thread.MyThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/")
public class RoomManagerController {

    private final EventRepository repository;

    @Autowired
    public RoomManagerController(EventRepository repository) {
        this.repository = repository;
    }

    @PostConstruct
    public void workingThread(){
        MyThread thread=new MyThread(repository);
        Thread t=new Thread(thread);
        t.start();

    }


    @PostMapping("/requestRoom")
    public ResponseEntity<Void> requestRoom(@RequestBody Event event) throws NegativeException, HourException {
        if(event.getPersonCount()<0)
            throw new NegativeException();
        if(event.getPersonCount()<=15) {
            event.setRoomName("Maple");
            if(timeCheck(event).isEmpty()) {
                repository.insert(event);
                return new ResponseEntity<Void>(HttpStatus.OK);
            }

        }else if(event.getPersonCount()<=25){
            event.setRoomName("Cluj");
            if(timeCheck(event).isEmpty()) {
                repository.insert(event);
                return new ResponseEntity<Void>(HttpStatus.OK);
            }
        }else if(event.getPersonCount()<=50){
            event.setRoomName("D1");
            if(timeCheck(event).isEmpty()) {
                repository.insert(event);
                return new ResponseEntity<Void>(HttpStatus.OK);
            }
        }

        return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);

    }

    @GetMapping("/{roomName}/todayEvents")
    public List<Event> getEventsPerDay(@PathVariable String roomName){
        Date currentDate = new Date();
        currentDate.getCurrentDate();
        return repository.findAllByRoomNameAndEventDateOrderByStartingTime(roomName,currentDate);
    }

    @GetMapping("/{roomName}/events")
    public List<Event> getEvents(@PathVariable String roomName){
        return repository.findAllByRoomName(roomName);
    }


    private List<Event> timeCheck(Event event){
        List<Event> events=repository.findAllByRoomNameAndEventDateOrderByStartingTime(event.getRoomName(),event.getEventDate());
        Time currentTime = new Time();
        currentTime.getCurrentTime();
        List<Event> overlappingEvents=new ArrayList<>();
        if(!event.getStartingTime().greaterThan(currentTime)){
            overlappingEvents.add(event);
            return overlappingEvents;
        }
        for(Event e:events){
            if(e.getStartingTime().equals(event.getStartingTime())
                    || e.getEndingTime().equals(event.getEndingTime())
                )
                overlappingEvents.add(e);
    
            if(event.getStartingTime().greaterThan(e.getStartingTime())
                && e.getEndingTime().greaterThan(event.getEndingTime()))
                overlappingEvents.add(e);

            if(event.getStartingTime().greaterThan(e.getStartingTime())
                && e.getEndingTime().greaterThan(event.getStartingTime()))
                overlappingEvents.add(e);

            if(event.getEndingTime().greaterThan(e.getStartingTime())
                && e.getEndingTime().greaterThan(event.getEndingTime()))
                overlappingEvents.add(e);

            if(e.getStartingTime().greaterThan(event.getStartingTime())
                && event.getEndingTime().greaterThan(e.getEndingTime()))
                overlappingEvents.add(e);
        }

            return overlappingEvents;
    }


    @PostMapping("/delete/event")
    public String deleteEvent(@RequestParam String eventName){
    return "lala";
    }

    @PostMapping("/room/checkin")
    public  ResponseEntity<Void> checkIn(@RequestBody Event event){
        Event e=repository.findByEventNameAndEventAuthor(event.getEventName(),event.getEventAuthor());
        if(e==null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        e.checkIn();
        repository.save(e);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
