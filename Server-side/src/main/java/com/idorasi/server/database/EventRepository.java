package com.idorasi.server.database;

import com.idorasi.server.models.Date;
import com.idorasi.server.models.Event;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface EventRepository extends MongoRepository<Event,String> {

    List<Event> findAllByRoomNameAndEventDateOrderByStartingTime(String roomName,Date eventDate);

    Event findByEventNameAndEventAuthor(String eventName,String eventAuthor);

    List<Event> findAllByRoomName(String roomName);

    List<Event> findAllByEventDate(Date date);

    List<Event> findAllByEventDateIsLessThanEqual(Date date);


}
