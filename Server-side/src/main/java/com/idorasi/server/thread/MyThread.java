package com.idorasi.server.thread;


import com.idorasi.server.database.EventRepository;
import com.idorasi.server.models.Date;
import com.idorasi.server.models.Event;

import java.util.List;

public class MyThread implements Runnable {

    private EventRepository repository;
    private Date date = new Date();

    public MyThread(EventRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run() {
        for(; ;){
            date.getCurrentDate();
            List<Event> events = repository.findAllByEventDateIsLessThanEqual(date);
            if(!events.isEmpty()) {
                for (Event e : events) {
                    if (e.verifyExpiration()) {
                        repository.delete(e);
                        System.out.println(e.getEventName() + " has been deleted");
                    }
                }
            }
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

}
